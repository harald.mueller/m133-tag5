<?php session_start(); ?>
<!DOCTYPE html>
<html >
  <head>
    <meta charset="UTF-8">
    <title>Login</title> 
    <link rel="stylesheet" href="view/css/anmeldung.css">
  </head>
  <body>
  
    <a href="#" class="button" id="toggle-login">Login</a>
	
	<div id="login">
	  <div id="triangle"></div>
	  <h1>Login</h1>
	  <form action="control/anmeldecheck_mit_userdaten.php" method="post">
	    <input type="email" name="benutzer" placeholder="Email" />
    	  <?php if (isset($_SESSION['initialuser_ist_angelegt']) AND $_SESSION['initialuser_ist_angelegt'] == TRUE) {
    	            echo "<p> Der initiale User wurde angelegt! Erst jetzt kann man sich einloggen.</p>";
    	        } 
    	  ?>
	    <input type="password" name="password" placeholder="Password" />
	    <input type="submit" value="Login" />
	  </form>
	</div>

	<script src="control/js/anmeldung.js"></script>
	         
  </body>
</html>