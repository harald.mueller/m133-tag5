<?php session_start();
unset($_SESSION['initialuser_ist_angelegt']);
$_SESSION['fileNameUsers'] =  "data/users.json";

$fileName = "../".$_SESSION['fileNameUsers'];
if (file_exists($fileName)) {
    $json_data = file_get_contents($fileName);
    $userListe = json_decode($json_data, true);
    
    $benutzer = "";//gesichtertes Zuweisen von User
    if (isset($_REQUEST['benutzer'])) {
        $benutzer = htmlentities($_REQUEST['benutzer']);
    }
    $password = "";//gesichtertes Zuweisen von Passwort
    if (isset($_REQUEST['password'])) {
        $password = htmlentities($_REQUEST['password']);
    }
    // wenn uid & pwd gefüllt..
    if (( strlen($benutzer) > 0 ) AND ( strlen($password) > 0 ))  {
        if (array_key_exists($benutzer, $userListe) == FALSE) {
            echo "<br>Benutzer nicht bekannt!";            
        } else {
            //Benutzer ist bekannt
            //Checken ob das Passwort stimmt
            $gespeichertesPasswort = $userListe[$benutzer]["Pass"];
            $passHash = password_hash("Administrator", PASSWORD_BCRYPT);
            
            if (password_verify($password, $gespeichertesPasswort)) {
                // Valides Passwort
                $_SESSION['benutzer'] = $benutzer;
                $_SESSION['password'] = $password;
                $_SESSION['kuerzel'] = $userListe[$benutzer]["Krzl"];
                $_SESSION['usergruppe'] = $userListe[$benutzer]["UGrp"];
                $_SESSION['loged-in'] = TRUE;
                
                if ($userListe[$benutzer]["UGrp"] == "ADMIN") {
                    header("Location: ../userliste.php");
                } else {
                    header("Location: ../stundenliste.php");
                }
            } else {
                echo '<br>Invalides Passwort.';
            }
        }
        echo "<br><br><a href='../anmeldung_mit_userdaten.php'>zurück</a>";
    } else {
        header("Location: ../anmeldung_mit_userdaten.php");
    }
    
} else {
    // Die Userliste-Datei fehlt. Es werden Initialdaten gespeichert.
    // erster (Initial-)User (=Admin)
    $passwortHash = password_hash("Administrator", PASSWORD_BCRYPT);
    $userArray["admin@m133.ch"] = array(
        "Krzl" => "ADM",
        "Name" => "Administrator",
        "UGrp" => "ADMIN",
        "Pass" => $passwortHash);
    // zweiter (Initial-)User aus Gruppe USER
    $passwortHash = password_hash("Penner", PASSWORD_BCRYPT);
    $userArray["harald.mueller@tbz.ch"] = array(
        "Krzl" => "MUH",
        "Name" => "Harald G. Mueller",
        "UGrp" => "USER",
        "Pass" => $passwortHash);
    
    //Verzeichnis 'data' anlegen inkl. Berechtigung (hier die maximale 0777)
    mkdir("../data", 0777);
    
    $json_data = json_encode($userArray, JSON_PRETTY_PRINT);
    $isOK = file_put_contents($fileName, $json_data);

    $_SESSION['initialuser_ist_angelegt'] = TRUE;
    
    header("Location: ../anmeldung_mit_userdaten.php");
}
?>




