
				<!-- Footer -->
					<footer id="footer">
						<div class="inner">
							<ul class="icons">
								<li><a href="https://www.facebook.com/harald.g.mueller" class="icon alt fa-facebook"><span class="label">Facebook</span></a></li>
								<li><a href="https://www.instagram.com/harald.g.mueller/?hl=de" class="icon alt fa-instagram"><span class="label">Instagram</span></a></li>
								<li><a href="https://gitlab.com/harald.mueller/haraldmueller.bplaced.net" class="icon alt fa-github"><span class="label">GitHub</span></a></li>
								<li><a href="https://www.linkedin.com/in/harald-müller-69314059/" class="icon alt fa-linkedin"><span class="label">LinkedIn</span></a></li>
							</ul>
							<ul class="copyright">
								<li>&copy; hm</li><li>Design: <a href="https://html5up.net">HTML5 UP</a></li>
							</ul>
						</div>
					</footer>

			</div>

		<!-- Scripts -->
			<script src="../../control/js/jquery.min.js"></script>
			<script src="../../control/js/jquery.scrolly.min.js"></script>
			<script src="../../control/js/jquery.scrollex.min.js"></script>
			<script src="../../control/js/skel.min.js"></script>
			<script src="../../control/js/util.js"></script>
			<script src="../../control/js/main.js"></script>

	</body>
</html>